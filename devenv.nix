{ pkgs, ... }:

{
  packages = with pkgs; [
    clippy
    rust-analyzer
    openssl
    trunk
  ];

  languages.rust.enable = true;

  languages.rust.packages.rustc = (pkgs.rust-bin.stable.latest.default.override {
    targets = [ "wasm32-unknown-unknown" ];
  });

  pre-commit.hooks = {
    clippy.enable = true;
  };
}
