use std::io::Cursor;

use seed::*;
use seed::prelude::*;
use seed::prelude::web_sys::{HtmlInputElement, InputEvent, HtmlCanvasElement};
use tracing::info;

use plotters::prelude::*;
use plotters_canvas::CanvasBackend;

use abif::Abif;


fn init(_: Url, _: &mut impl Orders<Msg>) -> Model {
    Model {
        canvas: ElRef::<HtmlCanvasElement>::default(),
        abif: None,
    }
}

struct Model {
    canvas: ElRef<HtmlCanvasElement>,
    abif: Option<Abif>,
}

enum Msg {
    ReadFile(web_sys::Event),
    BytesRead(Abif),
}

fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>) {
    match msg {
        Msg::ReadFile(ev) => {
            let input = ev.unchecked_into::<InputEvent>();
            let el: HtmlInputElement = input.target().unwrap().unchecked_into();

            if let Some(files) = el.files() {
                let file = files.get(0).unwrap();

                orders.skip().perform_cmd(async {
                    let bytes = gloo::file::futures::read_as_bytes(&file.into()).await.unwrap();
                    let cursor = Cursor::new(bytes);
                    let abif = Abif::read(cursor).unwrap();

                    Msg::BytesRead(abif)
                });
            }
        },
        Msg::BytesRead(abif) => {
            info!(meta = ?abif.metadata());
            chromatograph(&model.canvas.get().unwrap(), &abif);
            model.abif = Some(abif);
        }
    }
}

fn view(model: &Model) -> Node<Msg> {
    div![
        div![
            input![
                attrs! {
                    At::Type => "file"
                },
                ev(Ev::Change, |ev| Msg::ReadFile(ev))
            ],
        ],

        div![
            style![
                St::OverflowY => "auto",
            ],
            canvas![
                el_ref(&model.canvas),
                attrs![
                    At::Id => "chromatograph",
                    At::Width => px(8000),
                    At::Height => px(300),
                ],
                style![
                    St::Border => "1px solid black",
                ],
            ],
        ],

        div![
            h1!["Metadata"],
            metadata(model),
        ]
    ]
}

fn metadata(model: &Model) -> Option<Node<Msg>> {
    if let Some(abif) = &model.abif {
        let metadata = abif.metadata();

        Some(table![
            attrs!{ At::Class => "table" },
            thead![
                tr![
                    td![strong!["Versions"]]
                ]
            ],
            tr![
                td!["Data collection"],
                td![metadata.software_versions.data_collection],
            ],
            tr![
                td!["Sizecaller"],
                td![metadata.software_versions.sizecaller],
            ],
            tr![
                td!["Firmware"],
                td![metadata.software_versions.firmware],
            ],

            thead![
                tr![
                    td![strong!["Analysis protocol"]]
                ]
            ],
            tr![
                td!["Settings name"],
                td![metadata.analysis_protocol.settings_name],
            ],
            tr![
                td!["Settings version"],
                td![metadata.analysis_protocol.settings_version],
            ],

            thead![
                tr![
                    td![strong!["Peak spacing"]]
                ]
            ],
            tr![
                td!["Average used in last analysis"],
                td![metadata.peak_spacing.avg_used_in_last_analysis.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Average last calculated"],
                td![metadata.peak_spacing.avg_last_calculated.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Basecaller name"],
                td![metadata.peak_spacing.basecaller_name],
            ],

            thead![
                tr![
                    td![strong!["Separation medium"]]
                ]
            ],
            tr![
                td!["Lot number"],
                td![metadata.separation_medium.lot_number.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Lot expiry"],
                td![metadata.separation_medium.lot_expiry.and_then(|v| Some(v.to_string()))],
            ],

            thead![
                tr![
                    td![strong!["Run"]]
                ]
            ],
            tr![
                td!["Name"],
                td![metadata.run.name.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Temperature"],
                td![metadata.run.temperature.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Start date"],
                td![metadata.run.start_date.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Start time"],
                td![metadata.run.start_time.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["End date"],
                td![metadata.run.end_date.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["End time"],
                td![metadata.run.end_time.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Protocol name"],
                td![metadata.run.protocol_name.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Protocol version"],
                td![metadata.run.protocol_version.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Module name"],
                td![metadata.run.module_name.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Module version"],
                td![metadata.run.module_version.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Module schema version"],
                td![metadata.run.module_schema_version.and_then(|v| Some(v.to_string()))],
            ],
            // tr![
            //     td!["Module XML"],
            //     td![metadata.run.module_xml.and_then(|v| Some(v.to_string()))],
            // ],

            thead![
                tr![
                    td![strong!["Data collection"]]
                ]
            ],
            tr![
                td!["Start date"],
                td![metadata.data_collection.start_date.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Start time"],
                td![metadata.data_collection.start_time.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["End date"],
                td![metadata.data_collection.end_date.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["End time"],
                td![metadata.data_collection.end_time.and_then(|v| Some(v.to_string()))],
            ],

            thead![
                tr![
                    td![strong!["Results group"]]
                ]
            ],
            tr![
                td!["Name"],
                td![metadata.results_group.name.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Owner"],
                td![metadata.results_group.owner.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Comment"],
                td![metadata.results_group.comment.and_then(|v| Some(v.to_string()))],
            ],

            thead![
                tr![
                    td![strong!["Plate"]]
                ]
            ],
            tr![
                td!["Type"],
                td![metadata.plate.r#type.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Size"],
                td![metadata.plate.size.and_then(|v| Some(v.to_string()))],
            ],

            thead![
                tr![
                    td![strong!["Instrument"]]
                ]
            ],
            tr![
                td!["Class"],
                td![metadata.instrument.class.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Family"],
                td![metadata.instrument.family.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Name"],
                td![metadata.instrument.name.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Parameters"],
                td![metadata.instrument.parameters.and_then(|v| Some(v.to_string()))],
            ],

            thead![
                tr![
                    td![strong!["Dyes"]]
                ]
            ],
            tr![
                td!["Total"],
                td![metadata.dyes.total.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Name"],
                td![metadata.dyes.name.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Dye 1 Name"],
                td![metadata.dyes.dye1.as_ref().and_then(|v| v.name.clone())],
            ],
            tr![
                td!["Dye 1 Wavelength"],
                td![metadata.dyes.dye1.and_then(|v| v.wavelength)],
            ],
            tr![
                td!["Dye 2 Name"],
                td![metadata.dyes.dye2.as_ref().and_then(|v| v.name.clone())],
            ],
            tr![
                td!["Dye 2 Wavelength"],
                td![metadata.dyes.dye2.and_then(|v| v.wavelength)],
            ],
            tr![
                td!["Dye 3 Name"],
                td![metadata.dyes.dye3.as_ref().and_then(|v| v.name.clone())],
            ],
            tr![
                td!["Dye 3 Wavelength"],
                td![metadata.dyes.dye3.and_then(|v| v.wavelength)],
            ],
            tr![
                td!["Dye 4 Name"],
                td![metadata.dyes.dye4.as_ref().and_then(|v| v.name.clone())],
            ],
            tr![
                td!["Dye 4 Wavelength"],
                td![metadata.dyes.dye4.and_then(|v| v.wavelength)],
            ],

            thead![
                tr![
                    td![strong!["Container"]]
                ]
            ],
            tr![
                td!["ID"],
                td![metadata.container.id.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Name"],
                td![metadata.container.name.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Owner"],
                td![metadata.container.owner.and_then(|v| Some(v.to_string()))],
            ],

            thead![
                tr![
                    td![strong!["Analysis"]]
                ]
            ],
            tr![
                td!["Return code"],
                td![metadata.analysis.return_code.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Adaptive processing"],
                td![metadata.analysis.adaptive_processing.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Starting scan first"],
                td![metadata.analysis.starting_scan_first.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Starting scan last"],
                td![metadata.analysis.starting_scan_last.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Ending scan first"],
                td![metadata.analysis.ending_scan_first.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Ending scan last"],
                td![metadata.analysis.ending_scan_last.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Reference scan first"],
                td![metadata.analysis.ref_scan_first.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Reference scan last"],
                td![metadata.analysis.ref_scan_last.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Basecaller completed"],
                td![metadata.analysis.basecaller_completed.and_then(|v| Some(v.to_string()))],
            ],


            thead![
                tr![
                    td![strong!["Other"]]
                ]
            ],
            tr![
                td!["Scans"],
                td![metadata.scans.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Sample name"],
                td![metadata.sample_name.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Dye signal strength"],
                td![metadata.dye_signal_strength.and_then(|v| Some(format!("{v:?}").to_string()))],
            ],
            tr![
                td!["Complemented"],
                td![metadata.complemented.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Pixel bin size"],
                td![metadata.pixel_bin_size.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Baseline noise"],
                td![metadata.baseline_noise.and_then(|v| Some(format!("{v:?}").to_string()))],
            ],
            tr![
                td!["Total capillaries"],
                td![metadata.total_capillaries.and_then(|v| Some(v.to_string()))],
            ],
            tr![
                td!["Averaged columns"],
                td![metadata.averaged_columns.and_then(|v| Some(v.to_string()))],
            ],
        ])
    } else {
        None
    }
}


pub fn main() {
    console_error_panic_hook::set_once();
    tracing_wasm::set_as_global_default();

    App::start("app", init, update, view);
}


fn chromatograph(canvas: &HtmlCanvasElement, abif: &Abif) {
    let backend = CanvasBackend::new(&canvas.id()).expect("cannot find canvas");
    let root = backend.into_drawing_area();
    root.fill(&WHITE).unwrap();

    let a = abif.a_analyzed().unwrap();
    let c = abif.c_analyzed().unwrap();
    let g = abif.g_analyzed().unwrap();
    let t = abif.t_analyzed().unwrap();

    let mut chart = ChartBuilder::on(&root)
        .margin(20)
        .x_label_area_size(30)
        .y_label_area_size(30)
        .build_cartesian_2d(0i32..(a.len() as i32), 0i32..1500i32)
        .unwrap();

    chart.configure_mesh().draw().unwrap();

    chart.draw_series(LineSeries::new(
        a.into_iter().enumerate().map(|(i, v)| (i as i32, v)),
        &GREEN,
    )).unwrap();

    chart.draw_series(LineSeries::new(
        c.into_iter().enumerate().map(|(i, v)| (i as i32, v)),
        &BLUE,
    )).unwrap();

    chart.draw_series(LineSeries::new(
        g.into_iter().enumerate().map(|(i, v)| (i as i32, v)),
        &BLACK,
    )).unwrap();

    chart.draw_series(LineSeries::new(
        t.into_iter().enumerate().map(|(i, v)| (i as i32, v)),
        &RED,
    )).unwrap();

    root.present().unwrap();
}
